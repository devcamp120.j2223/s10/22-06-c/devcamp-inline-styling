import avatar from "./assets/images/48.jpg";

const style = {
  container: {
    width: "700px",
    margin: "0 auto",
    textAlign: "center",
    marginTop: "100px",
    border: "1px solid #ddd",
    padding: "30px"
  },
  avatar: {
    borderRadius: "50%",
    marginTop: "-90px",
    width: "100px",
    marginBottom: "30px"
  },
  quote: {
    fontSize: "17px",
    marginBottom: "20px"
  },
  name: {
    fontWeight: "bold"
  }
}

function App() {
  return (
    <div style={style.container}>
      <div>
        <img src={avatar} alt="avatar" style={style.avatar}/>
      </div>
      <div style={style.quote}>
        This is one of the best developer blogs on the planet! I read it daily to improve my skills.
      </div>
      <div>
        <span style={style.name}>
          Tammy stevens
        </span>
        <span>
          &nbsp; * &nbsp;Front End Developer
        </span>
      </div>
    </div>
  );
}

export default App;
